
import java.util.Scanner;
import java.util.Random;

/**
 * Juego tic-tac-toe
 * 
 * Método principal "play"  Curso Java Salamanca Mayo/Junio 2018  Mini-Proyecto
 * 
 * @author David Gómez
 * @version 1.0
 * 
 */
class TictactoeMaquina {

    // constantes utilizadas en el código
    final String[] SIMBOLOS = new String[] { "-", "X", "O" };
    final int GANA1 = 1;
    final int GANA2 = 2;
    final int EMPATE = 0;
    final int SEGUIR = -1;
    // array 2D (matriz) de posiciones ganadoras
    final int[][] WINNERS = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 1, 4, 7 }, { 2, 5, 8 }, { 3, 6, 9 }, { 1, 5, 9 },
            { 7, 5, 3 } };

    // mapa de posiciones ocupadas, un array de 9 enteros indicando:
    // (0) posición libre (1) jugador1 (2) jugador2
    int[] map = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    // inicializamos keyboard como atributo de clase para poderlo utilizar en varios
    // métodos
    Scanner keyboard = new Scanner(System.in);

    /**
     * Método principal  Crea un bucle indefinido en que ejecuta los dos turnos, 
     * no para cada jugador Siempre empieza el jugador 1 En cada turno se llama al
     * método "turno" que pide la jugada y redibuja el mapa
     * 
     */

    public void play() {

        int respuesta;

        // dibujamos tablero por primera vez
        draw();

        do {
            // invocamos turno con número de jugador y recibimos respuesta
            respuesta = turno(1);
            // si respuesta es distinta de SEGUIR, saltamos el turno 2
            if (respuesta != SEGUIR)
                continue;
            // turno 2, ejecutamos y esperamos respuesta
            respuesta = turnomaquina(2);

        } while (respuesta == SEGUIR);

        // hemos salido del bucle, por tanto tenemos o ganador o empate
        // mostramos mensaje adecuado
        switch (respuesta) {
        case GANA1:
            System.out.println("Gana el jugador 1");
            break;
        case GANA2:
            System.out.println("Gana la maquina");
            break;
        default:
            System.out.println("Empate, no hay más posiciones");
            break;
        }

        // cerramos teclado, aunque no es imprescindible
        keyboard.close();
    }

    /**
     * método que ejecuta la jugada:  pide posición al jugador y devuelve r
     * sultado A COMPLETAR: debería verificar si la posición está ocupada, y en es
     * e caso volverla a pedir
     */
    int turno(int jugador) {
        int resp;
        int posicion=0;
        boolean comprobar=false;
        // mostramos pregunta y esperamos número introducido
        
        do {
            System.out.printf("Jugador %d: ", jugador);
             
        try {
            posicion = keyboard.nextInt();
            comprobar=false;
        } catch(Exception e) {
            System.out.println("***Dato incorrecto***");
                    keyboard.next();
                    comprobar=true;
        }
        }while(comprobar==true);
        // numero >1 y >9
        // posicion este a 0 con getMap
        //try catch aqui
        if (posicion > 9 || posicion < 1) {
            System.out.println("Valor incorrecto");
            // keyboard.next();
            resp = turno(jugador);
            return resp;

        }

        if (getMap(posicion) != 0) {
            System.out.println("Posicion ocupada");
            // keyboard.next();
            resp = turno(jugador);
            return resp;
        }

        // establecemos posición en el mapa para jugador actual
        setMap(posicion, jugador);
        // mostramos mapa actualizado
        draw();

        // calcula respuesta que debe retornar
        if (winner(jugador)) {
            resp = (jugador == 1) ? GANA1 : GANA2;
        } else if (numZeros() == 0) {
            resp = EMPATE;
        } else {
            resp = SEGUIR;
        }

        return resp;
    }

    /**
     * método que ejecuta la jugada:  automatico
     * 
     */
    int turnomaquina(int jugador) {
        int resp;
        // mostramos pregunta y esperamos número introducido
        System.out.printf("Jugador Maquina ");
        // Random random = new Random();
        // int posicion = random.nextInt(9)+1;
        int posicion = posicionmaquina();

        // numero >1 y >9
        // posicion este a 0 con getMap

        if (getMap(posicion) != 0) {
            System.out.println("Posicion ocupada");
            // keyboard.next();
            resp = turnomaquina(jugador);
            return resp;
        }

        // establecemos posición en el mapa para jugador actual
        setMap(posicion, jugador);
        // mostramos mapa actualizado
        draw();

        // calcula respuesta que debe retornar
        if (winner(jugador)) {
            resp = (jugador == 1) ? GANA1 : GANA2;
        } else if (numZeros() == 0) {
            resp = EMPATE;
        } else {
            resp = SEGUIR;
        }

        return resp;
    }

    /**
     * método que verifica si en las posiciones actuales del tablero el jugador 
     * recibido es el ganador, y en este caso devolver true, de lo contrario false A
     * COMPLETAR: debe verificar realmente las jugadas! pista: utilizar el array
     * WINNERS...
     */
    boolean winner(int jugador) {
        // for return true;

        for (int i = 0; i < 8; i++) {
            int contador = 0;
            // if(getMap(WINNERS[i][0])&&(getMap(WINNERS[i][1])&& (getMap(WINNERS[i][2])

            for (int j = 0; j < 3; j++) {
                if (getMap(WINNERS[i][j]) == jugador) {
                    contador++;

                }
            }
            if (contador == 3) {
                return true;
            }
        }
        return false;
    }

    // devuelve el número de posiciones 0 que hay en el tablero
    // si no queda ninguna, significará que se ha terminado la partida (devuelve 0)
    int numZeros() {
        int zeros = 0;
        for (int i : map) {
            if (i == 0)
                zeros++;
        }
        return zeros;
    }

    // establece la posición del mapa al valor recibido (1/2 segun jugador)
    // IMPORTANTE restamos 1 a la posición puesto que las posiciones van de 1 a 9 
    //  el array de 0 a 8!
    void setMap(int posicion, int valor) {
        this.map[posicion - 1] = valor;
    }

    // devuelve el valor que hay en la posición recibida. 
    // restamos también una unidad a la posición!
    int getMap(int posicion) {
        return this.map[posicion - 1];
    }

    // muesta mapa en pantalla, MEJORABLE
    public void draw() {
        String l1 = this.SIMBOLOS[getMap(1)] + "  " + this.SIMBOLOS[getMap(2)] + "  " + this.SIMBOLOS[getMap(3)];
        String l2 = this.SIMBOLOS[getMap(4)] + "  " + this.SIMBOLOS[getMap(5)] + "  " + this.SIMBOLOS[getMap(6)];
        String l3 = this.SIMBOLOS[getMap(7)] + "  " + this.SIMBOLOS[getMap(8)] + "  " + this.SIMBOLOS[getMap(9)];
        System.out.println();
        System.out.println();
        System.out.println("3enRAYA");
        System.out.println("-------");
        System.out.println(l1);
        System.out.println(l2);
        System.out.println(l3);
        System.out.println("-------");

    }

    int posicionmaquina() {
         Random random = new Random();
         int posicion = random.nextInt(9)+1;
        
         int num = 0;
         for (int i = 0; i < 8; i++) {
                    int contador = 0;
                    // if(getMap(WINNERS[i][0])&&(getMap(WINNERS[i][1])&& (getMap(WINNERS[i][2])
        
                    for (int j = 0; j < 3; j++) {
                        if (getMap(WINNERS[i][j]) == 2) {
                            contador++;
                           
                        }
                    }
                    if (contador == 2) {
                        for (int z = 0; z < 3; z++) {
                            if (getMap(WINNERS[i][z]) == 0) {
                                num = WINNERS[i][z];
                                return num;
                            }
                           
                        }
                    }
                
                }
               
                for (int i = 0; i < 8; i++) {
                    int contador = 0;
                    // if(getMap(WINNERS[i][0])&&(getMap(WINNERS[i][1])&& (getMap(WINNERS[i][2])
        
                    for (int j = 0; j < 3; j++) {
                        if (getMap(WINNERS[i][j]) == 1) {
                            contador++;
                           
                        }
                    }
                    if (contador == 2) {
                        for (int z = 0; z < 3; z++) {
                            if (getMap(WINNERS[i][z]) == 0) {
                                num = WINNERS[i][z];
                                return num;
                            }
                           
                        }
                    }
                
                }        
            
                // for(int t=1;t<8;t++){
                //      if(getMap(t)==0){return getMap(t);}
                // return num;}return num;
                if(getMap(5)==0){
                    posicion=5;
                }
         return posicion;
 
    }

}
    //Todos los i, hasta return
                    // if (getMap(WINNERS[i][0]) == 0) {
                    //     num = WINNERS[i][0];
                    //     return num;
                    // }
                    // if (getMap(WINNERS[i][1]) == 0) {
                    //     num = WINNERS[i][1];
                    //     return num;
                    // }
                    // if (getMap(WINNERS[i][2]) == 0) {
                    //     num = WINNERS[i][2];
                    //     return num;
                    // }
                       //     for (int i = 0; i < 8; i++) {
    //         int contador = 0;
    //         // if(getMap(WINNERS[i][0])&&(getMap(WINNERS[i][1])&& (getMap(WINNERS[i][2])

    //         for (int j = 0; j < 3; j++) {
    //             if (getMap(WINNERS[i][j]) == 2) {
    //                 contador++;
                   
    //             }
    //         }
    //         if (contador == 2) {
    //             for (int z = 0; z < 3; z++) {
    //                 if (getMap(WINNERS[i][z]) == 0) {
    //                     num = WINNERS[i][z];
    //                     return num;
    //                 }
                   
    //             }
    //         }
    //     }
       
             
    //   return 6;
    //     // for(int t=1;t<8;t++){
    //     //      if(getMap(t)==0){return getMap(t);}
    //     // return num;}return num;