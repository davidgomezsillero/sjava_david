

class Animalitos{

    public static void main(String[] args) {
        
        Animal a = new Perro();
        Animal b = new Pajaro();
        Animal c = new Serpiente();
        Animal d = new PerroMutante();
       
       //Ejemplos
        a.duerme();
        ((Perro)a).ladra();

        //Perro cast
        //Perro p = (Perro) a;

        //perro
        System.out.println("Patas de perro " + a.getNumeroPatas());
        System.out.printf("Perro: " );
        ((Perro)a).ladra();
        //Pajaro
        System.out.println("Patas de pajaro " + b.getNumeroPatas());
        System.out.printf("Pajaro: " );
        ((Pajaro)b).vuela();
        //Serpiente
        System.out.println("Patas de serpiente " + c.getNumeroPatas());
        System.out.printf("Serpiente: " );
        ((Serpiente)c).repta();
        //PerroMutante
        System.out.println("Patas de Mutante " + d.getNumeroPatas());

        //Perro 2 patas por consiguiente perro mutante 3
        


    }

}