

class Persona {

    String nombre;
    int edad;

    Persona(String nombre, int edad) {
        this.nombre=nombre;
        this.edad=edad;
    }

    public String toString(){

        return "nombre: " + this.nombre + " edad: " + this.edad;

    }

}