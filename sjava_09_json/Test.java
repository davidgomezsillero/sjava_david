
//Package que es donde estará gson.jar (libresias, .class)
import com.google.gson.Gson;


public class Test {
    public static void main(String[] args) {
        Gson gson = new Gson();

        Persona p = new Persona("ricard", 22);
        //Otra forma
       // gson.toJson(p, System.out);
        
        String str = gson.toJson(p);  //clase Gson para convertir a string
        System.out.println(str);

        String js = "{\"nombre\":\"humbert\",\"edad\":22}";

        //Pasarle string y la clase para crear el objeto
        //Se lo pasa a esa persona
        Persona p2 = gson.fromJson(js, Persona.class);
        //Se comprueba la conversión
        System.out.println(p2.nombre);
        System.out.println(p2);

    }
}