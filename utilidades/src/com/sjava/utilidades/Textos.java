package com.sjava.utilidades;

public class Textos {

    public static String capitaliza(String in) {
        return in.substring(0,1).toUpperCase() + in.substring(1).toLowerCase();
    }
     public static String slug(String frase) {
         //quitar espacios
         String frasenueva = frase.replace(" ","_");
         return frasenueva;
     }
}
