var pieData = [{
		value: 30,
		color: "#0b82e7",
		highlight: "#0c62ab",
		label: "Google Chrome"
	},
	{
		value: 0,
		color: "#e3e860",
		highlight: "#a9ad47",
		label: "Android"
	},
	{
		value: 5,
		color: "white",
		highlight: "#b74865",
		label: "Firefox"
	},
	{
		value: 11,
		color: "#eb5d82",
		highlight: "#b74865",
		label: "Firefox"
	},
	{
		value: 10,
		color: "#5ae85a",
		highlight: "#42a642",
		label: "Internet Explorer"
	},
];


var ctx = document.getElementById("chart-area").getContext("2d");
var ctx2 = document.getElementById("chart-area2").getContext("2d");
var ctx3 = document.getElementById("chart-area3").getContext("2d");
var ctx4 = document.getElementById("chart-area4").getContext("2d");
window.myPie = new Chart(ctx).Pie(pieData);