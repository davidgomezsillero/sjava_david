<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.sjava.web.*" %>

<%

    Pista p = null;
            
    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");
    if (id==null){
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/padelUnion");
    }else{
        // recibimos id, puede ser que
        // 1) llegue por GET, desde list.jsp: mostraremos los datos para que puedan ser editados
        // 2) llegue por POST, junto con el resto de datos, para guardar los cambios

        //verificamos si la petición procede de un POST
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            // hemos recibido un POST, reccpilamos el resto de datos...
                
                int id_numerico = Integer.parseInt(id);
                String nombre = request.getParameter("nombre");
                int idcentro = Integer.parseInt(request.getParameter("idcentro"));
                String tipo = request.getParameter("tipo");
                String precio = request.getParameter("precio");
                //  prueba debug ... System.out.println(nombre);
                
               
                //creamos nuevo objeto alumno, que reemplazará al actual del mismo id
                p = new Pista(id_numerico, nombre, idcentro, tipo, precio);
                PistaController.guardar(p);
                //redirigimos navegador a la página list.jsp
                response.sendRedirect("/padelUnion/pista/list.jsp");
        } else {
            // hemos recibido un GET, solo tenemos un id
            // pedimos los datos del alumno para mostrarlos en el formulario de edifión
            p = PistaController.getId(Integer.parseInt(id));
            if (p==null){
                //no recibimos id, debe ser un error... volvemos a index
             response.sendRedirect("/padelUnion");
             }
        }
		
    }

%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion app</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelprueba/css/estilos.css">
</head>
<body>

<%@include file="/parts/menu.html"%>

<div class="container">

<div class="row">
<div class="col">
<h1>Editar pista</h1>
</div>
</div>


<div class="row">
<div class="col-md-8">


<form action="#" method="POST">
  <div class="form-group">
    <label for="nombreInput">Nombre de la pista</label>
    <input  name="nombre"  type="text" class="form-control" id="nombreInput" value="<%= p.getNombre() %> ">
  </div>

 <div class="form-group">
    <label for="idcentroInput">Centro de la pista</label>
    <select class="form-control" name="idcentro" type="text" class="form-control" id="idcentroInput" >
     <% for (Centro c : CentroController.getAll()){%>
      
       <option  value="<%=c.getId()%>"><%= c.getNombre()%></option>

         <%} %>
    </select>
  </div>
    <div class="form-group">
    <label for="tipoInput">Tipo</label>
    <input  name="tipo"  type="text" class="form-control" id="tipoInput" value="<%= p.getTipo() %> ">
  </div>

  <div class="form-group">
    <label for="precioInput">Precio</label>
    <input  name="precio"  type="text" class="form-control" id="precioInput" value="<%= p.getPrecio() %> ">
  </div>

 
  <!-- guardamos id en campo oculto! -->
    <input type="hidden" name="id" value="<%= p.getId() %>">
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>



</div>
</div>

</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>



</body>
</html>
