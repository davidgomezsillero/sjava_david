<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.sjava.web.*" %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelprueba/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelprueba/css/estilosdavid.css">
</head>
<body>

<%@include file="/parts/menu.html"%>


<div>
<h2>Valoración de tu Nivel de Padel</h2>
<p>Esta herramienta es para conocer<strong>tu nivel de padel.</strong> </p>
 <p>El sistema consiste en valorar el diferente grado de progresión en el control de diferentes tipos de golpe
 para ponderar de una forma lo más objetiva posible cual es tu nivel de juego.</p>
<p>Una de las grandes dudas que tenemos cuando empezamos a jugar a pádel y vamos mejorando nuestra técnica es conocer nuestro nivel real de juego,
 ya que sobre todo para jugar torneos y partidos con jugadores desconocidos, en general, garantiza el equilibro de los partidos y competiciones.
 Llegando a ser más divertidos y efectivos.</p>
<p>Las diferencias de criterio entre clubs así como también la percepción subjetiva que tenemos de nuestro nivel de juego de padel,
 genera situaciones en que acabamos jugando con jugadores con niveles superiores al nuestro o al revés, 
 jugando con personas que tiene un nivel muy inferior al que dicen tener.</p>
<p></p>
<p></p>
<h3>Sistemas de valoración</h3>
<p>Básicamente se utilizan tres sistemas de valoración dependiendo de los clubs o zonas en la que practiques Padel:</p>
<ul>
  <li><strong>El sistema de nivel de padel por letras:</strong> D,  C, C+, B, B+, A, (A) . Donde <strong>D</strong> corresponde a iniciación y<strong> A</strong> a profesional (máximo)</li>
  <li><strong>Sistema numérico</strong>: de nivel <strong>1</strong> hasta nivel <strong>7</strong>. 1 corresponde a iniciación y 7 a profesional</li>
  <li><strong>Sistema de Categorias:</strong>
  <ul>
    <li>Iniciación</li>
    <li>Intermedio</li>
    <li>Intermedio alto</li>
    <li>Avanzado</li>
    <li>Competición</li>
    <li>Profesional</li>
    </ul>
  </li>
</ul>
<p>Normalmente cuando empezamos a jugar a padel nuestro nivel es el de iniciación, 
aunque existen factores previos que pueden modificar nuestro nivel de inicio, 
como por ejemplo una experiencia previa en tenis, nuestra facilidad de aprencizaje, etc.</p>

<p><strong>Funcionamiento:</strong></p>
<p>En función de la experiencia, entrenamiento, partidos jugados, forma física, reflejos, 
experiencia en torneos y competiciones, nuestra progresión puede ser más rápida o 
más lenta y por lo tanto nuestro nivel de juego puede evolucionar de una forma u
 otra con respecto a otros jugadores</p>
<p>Utilizamos un cuestionario para que sea el propio jugador 
el que se autocalifique en diferentes facetas de la técnica del pádel y sus estilo de juego.</p>
<p>Cada pregunta dispone de varias opciones que equivalen a un grado de progreso del jugador.
 Una vez rellenado y enviado el formulario, la herramienta calcula mediante un alogoritmo,  
 unos valores númericos que finalemente tienen una traducción en un nivel de padel equivalente.
 Este algoritmo puede ser modificado, siempre que mejore la calidad del mismo con cualquiera de futuras propuestas</p>


<div class="alert alert-info" role="alert">
<p>Lógicamente para que el resultado sea fiable y preciso es necesario que el jugador / juagadora 
sea objetivo en sus respuestas y seleecione realmente aquel escenario que mejor define su situación 
real de juego en la pista.</p>
</div>
</div>
<a href="/padelUnion/calculanivel/edit.jsp" class="btn btn-sm btn-primary">Calcula mi nivel</a>

  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>



</body>
</html>
