<%@page contentType="text/html; charset=UTF-8" %>
<%@page import="com.sjava.web.*" %>

<%
    boolean datosOk;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo partido
    
        //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");
    
 
        String hora = request.getParameter("hora");
        String dia = request.getParameter("dia");
        String sitio = request.getParameter("sitio");
        String precio = request.getParameter("precio");
        int jugador1 = Integer.parseInt(request.getParameter("jugador1"));
        int jugador2 = Integer.parseInt(request.getParameter("jugador2"));
        int jugador3 = Integer.parseInt(request.getParameter("jugador3"));
        int jugador4 = Integer.parseInt(request.getParameter("jugador4"));

        // aquí verificaríamos que todo ok, y solo si todo ok hacemos guarda, 
        // por el momento lo damos por bueno...
        datosOk=true;
        if (datosOk){
            Partido p = new Partido(2, hora, dia, sitio, precio,jugador1,jugador2,jugador3,jugador4);
            PartidoController.guarda(p);
            // nos vamos a mostrar la lista, que ya contendrá el nuevo partido
            response.sendRedirect("/padelUnion/partido/list.jsp");
        }
    }


%>
<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnionApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelprueba/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelprueba/css/estilosdavid.css">
</head>
<body>

<%@include file="/parts/menu.html"%>

<div class="container">
<div class="row">
<div class="col">
<h1>Nuevo partido</h1>
</div>
</div>


<div class="row">
<div class="col-md-8">


<form action="#" method="POST">
  <div class="form-group">
    <label for="diaInput">Dia del partido</label>
    <input  name="dia"  type="text" class="form-control" id="diaInput" >
  </div>
 <div class="form-group">
    <label for="horaInput">Hora del partido</label>
    <input  name="hora"  type="text" class="form-control" id="horaInput">
  </div>
   <div class="form-group">
    <label for="sitioInput">Lugar del partido</label>
    <input  name="sitio"  type="text" class="form-control" id="sitioInput">
  </div>
   <div class="form-group">
    <label for="precioInput">Precio del partido</label>
    <input  name="precio"  type="text" class="form-control" id="precioInput">
  </div>

   <div class="form-group">
    <label for="jugador1Input">Reservar jugador</label>
     <select  name="jugador1" type="checkbox" class="form-control" id="idjugador1Input">
          <option value="0">Disponible</option>
          <option value="1">Ocupado</option>
   </select>
    <label for="jugador2Input">Reservar jugador</label>
    <select  name="jugador2" type="checkbox" class="form-control" id="idjugador2Input">
          <option value="0">Disponible</option>
          <option value="1">Ocupado</option>
   </select>
    <label for="jugador3Input">Reservar jugador</label>
    <select  name="jugador3" type="checkbox" class="form-control" id="idjugador3Input">
          <option value="0">Disponible</option>
          <option value="1">Ocupado</option>
   </select>
     <label for="jugador4Input">Reservar jugador</label>
     
    <select  name="jugador4" type="checkbox" class="form-control" id="idjugador4Input">
          <option value="0">Disponible</option>
          <option value="1">Ocupado</option>
   </select>
   
  </div>

    <button type="submit" class="btn btn-primary">Guardar</button>
</form>


</div>
</div>




    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>



</body>
</html>
