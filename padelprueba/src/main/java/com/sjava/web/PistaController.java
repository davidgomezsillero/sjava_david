package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class PistaController {
    private static List<Pista> listaPistas = new ArrayList<Pista>();
    private static int contador = 0;


    // getAll devuelve la lista completa
    public static List<Pista> getAll(){
        return listaPistas;
    }

    //getId devuelve un registro
    public static Pista getId(int id){
        for (Pista p : listaPistas) {
            if (p.getId()==id){
                return p;
            }
        }
        return null;
    }

    //getPistaCentro devuelve las pistas de un centro
    public static List<Pista> getPistasCentro(int idcentro){

            List<Pista> listaPistasCentro = new ArrayList<Pista>();
            for (Pista p : listaPistasCentro) {
                if(p.getIdcentro() == idcentro) {
                        listaPistasCentro.add(p);
                }
    
            }
     
        return listaPistasCentro;
    } 

    //guardar: guarda una pista
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void guardar(Pista pis) {
        if (pis.getId() == 0){
            contador++;
            pis.setId(contador);
            listaPistas.add(pis);
        } else {
            for (Pista p : listaPistas) {
                if (p.getId()==pis.getId()) {
                   
                    p.setNombre(pis.getNombre());
                    p.setIdcentro(pis.getIdcentro());
                    p.setPrecio(pis.getPrecio());
                    p.setTipo(pis.getTipo());
                  
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de centros
    public static int size() {
        return listaPistas.size();
    }


    // removeId elimina centro por id
    public static void removeId(int id){
        Pista borrar=null;
        for (Pista p : listaPistas) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaPistas.remove(borrar);
        }
    }

}