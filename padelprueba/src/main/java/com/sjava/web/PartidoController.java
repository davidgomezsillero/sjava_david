package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class PartidoController {
    private static List<Partido> listaPartidos = new ArrayList<Partido>();
    private static int contador = 0;


    // getAll devuelve la lista completa
    public static List<Partido> getAll(){
        return listaPartidos;
    }

    //getId devuelve un registro
    public static Partido getId(int id){
        for (Partido p : listaPartidos) {
            if (p.getId()==id){
                return p;
            }
        }
        return null;
    }

    //getMispartidos devuelve la lista de partidos donde esta el usuario
    public static List<Partido> getMispartidos(int idusuario){
        List<Partido> listaMispartidos = new ArrayList<Partido>();
        for (Partido p : listaPartidos) {
            if(p.getCreador()==idusuario || p.getJugador1()==idusuario ||
            p.getJugador2()==idusuario || p.getJugador3()==idusuario || p.getJugador4()==idusuario){
                    listaMispartidos.add(p);
            }

        }
        return listaMispartidos;
    }
   
    //guarda: guarda un partido
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void guarda(Partido p) {
        
        if (p.getId() == 0){
            contador++;
            p.setId(contador);
            p.setEstado(1);
            listaPartidos.add(p);
           //id usuario que lo crea
           
        } else {
            for (Partido par : listaPartidos) {
                if (par.getId()==p.getId()) {
                   
                    par.setCreador(p.getCreador());
                    par.setHora(p.getHora());
                    par.setDia(p.getDia());
                    par.setSitio(p.getSitio());
                    par.setPrecio(p.getPrecio());
                    par.setJugador1(p.getJugador1());
                    par.setJugador2(p.getJugador2());
                    par.setJugador3(p.getJugador3());
                    par.setJugador4(p.getJugador4());
                    if( p.getJugador1()==0 ||
                        p.getJugador2()==0 ||
                        p.getJugador3()==0 ||
                        p.getJugador4()==0){

                            par.setEstado(p.getEstado());
                    } else{
                        par.setEstado(2);
                    }
                        
                    break;
                }
            }
        }
        
    }
    
    // size devuelve numero de partidos
    public static int size() {
        return listaPartidos.size();
    }


    // removeId elimina partido por id
    public static void removeId(int id){
        Partido borrar=null;
        for (Partido p : listaPartidos) {
            if (p.getId()==id){
                borrar = p;
                break;
            }
        }
        if (borrar!=null) {
            listaPartidos.remove(borrar);
        }
    }

}