package com.sjava.web;

public class Centro {

    private int id;
    private String nombre;
    private String ubicacion;
    private String telefono;
    private String url;
    private String email;

    public Centro(String nombre, String ubicacion, String telefono, String url, String email) {
        this.nombre = nombre;
        this.ubicacion = ubicacion;
        this.telefono = telefono;
        this.url = url;
        this.email = email;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de CentroController
        // CentroController.nuevoCentro(this);
    }

    public Centro(int id, String nombre, String ubicacion, String telefono, String url, String email) {
        this.id = id;
        this.nombre = nombre;
        this.ubicacion = ubicacion;
        this.telefono = telefono;
        this.url = url;
        this.email = email;
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getEmail(){
        return this.email;
    }

    protected void setEmail(String email){
        this.email = email;
    }

    public String getTelefono(){
        return this.telefono;
    }
    
    protected void setTelefono(String telefono){
        this.telefono = telefono;
    }


    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }

    public String getUbicacion(){
        return this.ubicacion;
    }

    protected void setUbicacion(String ubicacion){
        this.ubicacion = ubicacion;
    }

    public String getUrl(){
        return this.url;
    }

    protected void setUrl(String url){
        this.url = url;
    }

   
    @Override
    public String toString() {
        return String.format("%s (%s)", this.nombre, this.email);
    }
  
}