package com.sjava.web;

public class Pista {

    private int id;
    private String nombre;
    private int idcentro;
    private String tipo;
    private String precio;

    public Pista(String nombre, int idcentro, String tipo, String precio) {
        this.nombre = nombre;
        this.idcentro = idcentro;
        this.tipo = tipo;
        this.precio = precio;

        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guardar" de PistaController
        // PistaController.nuevoPista(this);
    }

    public Pista(int id, String nombre, int idcentro, String tipo, String precio) {
        this.id = id;
        this.nombre = nombre;
        this.idcentro = idcentro;
        this.tipo = tipo;
        this.precio = precio;
       
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre){
        this.nombre = nombre;
    }

    public int getIdcentro(){
        return this.idcentro;
    }

    protected void setIdcentro(int idcentro){
        this.idcentro = idcentro;
    }

    public String getPrecio(){
        return this.precio;
    }
    
    protected void setPrecio(String precio){
        this.precio = precio;
    }


    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }

    public String getTipo(){
        return this.tipo;
    }

    protected void setTipo(String tipo){
        this.tipo = tipo;
    }

   
    @Override
    public String toString() {
        return String.format("%s (%s)", this.nombre, this.tipo);
    }
  
}