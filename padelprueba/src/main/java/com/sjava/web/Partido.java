package com.sjava.web;

public class Partido {

    private int id;
    private int creador;
    private String hora;
    private String dia;
    private String sitio;
    private String precio;
    private int estado;
    private int jugador1=0;
    private int jugador2=0;
    private int jugador3=0;
    private int jugador4=0;


    //Constructores
    public Partido(int creador, String hora, String dia, String sitio, String precio,
        int jugador1, int jugador2, int jugador3, int jugador4) {
        this.creador = creador;
        this.hora = hora;
        this.dia = dia;
        this.sitio = sitio;
        this.precio = precio;
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
        this.jugador3 = jugador3;
        this.jugador4 = jugador4;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de PartidoController
    }

    public Partido(int id, int creador,
        String hora, String dia, String sitio, String precio,
    int jugador1, int jugador2, int jugador3, int jugador4) {
        this.id = id;
        this.creador = creador;
        this.hora = hora;
        this.dia = dia;
        this.sitio = sitio;
        this.precio = precio;
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
        this.jugador3 = jugador3;
        this.jugador4 = jugador4;
    }

    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }
    public int getCreador() {
        return this.creador;
    }

    protected void setCreador(int creador){
        this.creador = creador;
    }

    public String getHora(){
        return this.hora;
    }

    protected void setHora(String hora){
        this.hora = hora;
    }

    public String getDia(){
        return this.dia;
    }

    protected void setDia(String dia){
        this.dia = dia;
    }

    public String getSitio(){
        return this.sitio;
    }
    
    protected void setSitio(String sitio){
        this.sitio = sitio;
    }

    public String getPrecio(){
        return this.precio;
    }
    
    protected void setPrecio(String precio){
        this.precio = precio;
    }

    public int getEstado(){
        return this.estado;
    }

    protected void setEstado(int estado){
        this.estado=estado;
    }
 
    public int getJugador1(){
        return this.jugador1;
    }

    protected void setJugador1(int jugador1){
        this.jugador1=jugador1;
    }
    public int getJugador2(){
        return this.jugador2;
    }

    protected void setJugador2(int jugador2){
        this.jugador2=jugador2;
    }
    public int getJugador3(){
        return this.jugador3;
    }

    protected void setJugador3(int jugador3){
        this.jugador3=jugador3;
    }
    public int getJugador4(){
        return this.jugador4;
    }

    protected void setJugador4(int jugador4){
        this.jugador4=jugador4;
    }
    // @Override
    // public String toString() {
    //     return String.format("%d ", this.id);
    // }
  
}