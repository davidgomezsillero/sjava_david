package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

public class CentroController {
    private static List<Centro> listaCentros = new ArrayList<Centro>();
    private static int contador = 0;


    // getAll devuelve la lista completa
    public static List<Centro> getAll(){
        return listaCentros;
    }

    //getId devuelve un registro
    public static Centro getId(int id){
        for (Centro c : listaCentros) {
            if (c.getId()==id){
                return c;
            }
        }
        return null;
    }
   
    //guardar: guarda un centro
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void guardar(Centro cen) {
        if (cen.getId() == 0){
            contador++;
            cen.setId(contador);
            listaCentros.add(cen);
        } else {
            for (Centro c : listaCentros) {
                if (c.getId()==cen.getId()) {
                   
                    c.setNombre(cen.getNombre());
                    c.setUbicacion(cen.getUbicacion());
                    c.setTelefono(cen.getTelefono());
                    c.setUrl(cen.getUrl());
                    c.setEmail(cen.getEmail());
                  
                    break;
                }
            }
        }
        
    }

    // size devuelve numero de centros
    public static int size() {
        return listaCentros.size();
    }


    // removeId elimina centro por id
    public static void removeId(int id){
        Centro borrar=null;
        for (Centro c : listaCentros) {
            if (c.getId()==id){
                borrar = c;
                break;
            }
        }
        if (borrar!=null) {
            listaCentros.remove(borrar);
        }
    }

}