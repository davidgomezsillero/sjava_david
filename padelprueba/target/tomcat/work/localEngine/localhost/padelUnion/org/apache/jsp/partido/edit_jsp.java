package org.apache.jsp.partido;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.sjava.web.*;

public final class edit_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/parts/menu.html");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");


    Partido p = null;
            
    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");
    if (id==null){
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/padelUnion");
    }else{
        // recibimos id, puede ser que
        // 1) llegue por GET, desde list.jsp: mostraremos los datos para que puedan ser editados
        // 2) llegue por POST, junto con el resto de datos, para guardar los cambios
       
        //verificamos si la petición procede de un POST
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            // hemos recibido un POST, reccpilamos el resto de datos...
                
            int id_numerico = Integer.parseInt(id);
            int creador = 2; 
            String hora = request.getParameter("hora");
            String dia = request.getParameter("dia");
            String sitio = request.getParameter("sitio");
            String precio = request.getParameter("precio");
            int jugador1 = Integer.parseInt(request.getParameter("jugador1"));
            int jugador2 = Integer.parseInt(request.getParameter("jugador2"));
            int jugador3 = Integer.parseInt(request.getParameter("jugador3"));
            int jugador4 = Integer.parseInt(request.getParameter("jugador4"));
                    
                //creamos nuevo objeto alumno, que reemplazará al actual del mismo id
                p = new Partido(id_numerico, creador,
            hora,  dia,  sitio,  precio,
            jugador1,  jugador2,  jugador3,  jugador4);
                PartidoController.guarda(p);
                //redirigimos navegador a la página list.jsp
                response.sendRedirect("/padelUnion/partido/list.jsp");
        } else {
            // hemos recibido un GET, solo tenemos un id
            // pedimos los datos del partido para mostrarlos en el formulario de edición
            p = PartidoController.getId(Integer.parseInt(id));
            if (p==null){
                //no recibimos id, debe ser un error... volvemos a index
             response.sendRedirect("/padelUnion");
             }
        }
		
    }


      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"es-ES\">\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("    <title>PadelUnionApp</title>\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css\" integrity=\"sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4\" crossorigin=\"anonymous\">\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"/padelprueba/css/estilos.css\">\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"/padelprueba/css/estilosdavid.css\">\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<nav class=\"navbar navbar-expand-lg  navbar-dark bg-primary\">\r\n");
      out.write("    <a class=\"navbar-brand\" href=\"#\">PadelUnionApp</a>\r\n");
      out.write("    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n");
      out.write("      <span class=\"navbar-toggler-icon\"></span>\r\n");
      out.write("    </button>\r\n");
      out.write("  \r\n");
      out.write("    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n");
      out.write("      <ul class=\"navbar-nav mr-auto\">\r\n");
      out.write("        <li class=\"nav-item active\">\r\n");
      out.write("          <a class=\"nav-link\" href=\"/padelUnion\">Inicio</a>\r\n");
      out.write("        </li>\r\n");
      out.write("    \r\n");
      out.write("        <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            CalculaTest\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/padelUnion/calculanivel/edit.jsp\">Listado</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("\r\n");
      out.write("        <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            Partidos\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/padelUnion/partido/list.jsp\">Listado</a>\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/padelUnion/partido/create.jsp\">Nuevo Partido</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("\r\n");
      out.write("      \r\n");
      out.write("      </ul>\r\n");
      out.write("   \r\n");
      out.write("    </div>\r\n");
      out.write("  </nav>\r\n");
      out.write("  ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"container\">\r\n");
      out.write("\r\n");
      out.write("<div class=\"row\">\r\n");
      out.write("<div class=\"col\">\r\n");
      out.write("<h1>Editar partido</h1>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"row\">\r\n");
      out.write("<div class=\"col-md-8\">\r\n");
      out.write("<form action=\"#\" method=\"POST\">\r\n");
      out.write("  <div class=\"form-group\">\r\n");
      out.write("    <label for=\"diaInput\">Dia del partido</label>\r\n");
      out.write("    <input  name=\"dia\"  type=\"text\" class=\"form-control\" id=\"diaInput\" value=\"");
      out.print( p.getDia() );
      out.write("\" >\r\n");
      out.write("  </div>\r\n");
      out.write(" <div class=\"form-group\">\r\n");
      out.write("    <label for=\"horaInput\">Hora del partido</label>\r\n");
      out.write("    <input  name=\"hora\"  type=\"text\" class=\"form-control\" id=\"horaInput\" value=\"");
      out.print( p.getHora() );
      out.write("\">\r\n");
      out.write("  </div>\r\n");
      out.write("   <div class=\"form-group\">\r\n");
      out.write("    <label for=\"sitioInput\">Lugar del partido</label>\r\n");
      out.write("    <input  name=\"sitio\"  type=\"text\" class=\"form-control\" id=\"sitioInput\" value=\"");
      out.print( p.getHora() );
      out.write("\">\r\n");
      out.write("  </div>\r\n");
      out.write("   <div class=\"form-group\">\r\n");
      out.write("    <label for=\"precioInput\">Precio del partido</label>\r\n");
      out.write("    <input  name=\"precio\"  type=\"text\" class=\"form-control\" id=\"precioInput\" value=\"");
      out.print( p.getPrecio() );
      out.write("\">\r\n");
      out.write("  </div>\r\n");
      out.write("\r\n");
      out.write("   <div class=\"form-group\">\r\n");
      out.write("    <label for=\"jugador1Input\">Reservar jugador</label>\r\n");
      out.write("     <select  name=\"jugador1\" type=\"checkbox\" class=\"form-control\" id=\"idjugador1Input\">\r\n");
      out.write("        ");
 if(p.getJugador1()==0){
      out.write("\r\n");
      out.write("          <option selected value=\"0\">Disponible</option>\r\n");
      out.write("          <option value=\"1\">Ocupado</option>\r\n");
      out.write("        ");
} else  {
      out.write("\r\n");
      out.write("         <option value=\"0\">Disponible</option>\r\n");
      out.write("          <option selected value=\"1\">Ocupado</option>\r\n");
      out.write("          ");
}
      out.write("\r\n");
      out.write("   </select>\r\n");
      out.write("    <label for=\"jugador2Input\">Reservar jugador</label>\r\n");
      out.write("    <select  name=\"jugador2\" type=\"checkbox\" class=\"form-control\" id=\"idjugador2Input\">\r\n");
      out.write("        ");
 if(p.getJugador2()==0){
      out.write("\r\n");
      out.write("          <option selected value=\"0\">Disponible</option>\r\n");
      out.write("          <option value=\"1\">Ocupado</option>\r\n");
      out.write("        ");
} else  {
      out.write("\r\n");
      out.write("         <option value=\"0\">Disponible</option>\r\n");
      out.write("          <option selected value=\"1\">Ocupado</option>\r\n");
      out.write("          ");
}
      out.write("\r\n");
      out.write("   </select>\r\n");
      out.write("    <label for=\"jugador3Input\">Reservar jugador</label>\r\n");
      out.write("    <select  name=\"jugador3\" type=\"checkbox\" class=\"form-control\" id=\"idjugador3Input\">\r\n");
      out.write("        ");
 if(p.getJugador3()==0){
      out.write("\r\n");
      out.write("          <option selected value=\"0\">Disponible</option>\r\n");
      out.write("          <option value=\"1\">Ocupado</option>\r\n");
      out.write("        ");
} else  {
      out.write("\r\n");
      out.write("         <option value=\"0\">Disponible</option>\r\n");
      out.write("          <option selected value=\"1\">Ocupado</option>\r\n");
      out.write("          ");
}
      out.write("\r\n");
      out.write("   </select>\r\n");
      out.write("     <label for=\"jugador4Input\">Reservar jugador</label>\r\n");
      out.write("     \r\n");
      out.write("    <select  name=\"jugador4\" type=\"checkbox\" class=\"form-control\" id=\"idjugador4Input\">\r\n");
      out.write("        ");
 if(p.getJugador4()==0){
      out.write("\r\n");
      out.write("          <option selected value=\"0\">Disponible</option>\r\n");
      out.write("          <option value=\"1\">Ocupado</option>\r\n");
      out.write("        ");
} else  {
      out.write("\r\n");
      out.write("         <option value=\"0\">Disponible</option>\r\n");
      out.write("          <option selected value=\"1\">Ocupado</option>\r\n");
      out.write("          ");
}
      out.write("\r\n");
      out.write("   </select>\r\n");
      out.write("   \r\n");
      out.write("  </div>\r\n");
      out.write("<!-- guardamos id en campo oculto! -->\r\n");
      out.write("    <input type=\"hidden\" name=\"id\" value=\"");
      out.print( p.getId() );
      out.write("\">\r\n");
      out.write("    <button type=\"submit\" class=\"btn btn-primary\">Guardar</button>\r\n");
      out.write("</form>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("    <!-- jQuery first, then Popper.js, then Bootstrap JS -->\r\n");
      out.write("    <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js\" integrity=\"sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js\" integrity=\"sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
