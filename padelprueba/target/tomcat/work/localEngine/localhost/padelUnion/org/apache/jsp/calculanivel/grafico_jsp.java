package org.apache.jsp.calculanivel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.sjava.web.*;

public final class grafico_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/parts/menu.html");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html; charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("                                                               \r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"es-ES\">\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("    <title>PadelUnion App</title>\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css\" integrity=\"sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4\" crossorigin=\"anonymous\">\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"/padelprueba/css/estilos.css\">\r\n");
      out.write("     <link rel=\"stylesheet\" type=\"text/css\" href=\"/padelprueba/css/estilosdavid.css\"> \r\n");
      out.write("    \r\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.0.13/css/all.css\" integrity=\"sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp\" crossorigin=\"anonymous\">\r\n");
      out.write("    \r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<nav class=\"navbar navbar-expand-lg  navbar-dark bg-primary\">\r\n");
      out.write("    <a class=\"navbar-brand\" href=\"#\">PadelUnionApp</a>\r\n");
      out.write("    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n");
      out.write("      <span class=\"navbar-toggler-icon\"></span>\r\n");
      out.write("    </button>\r\n");
      out.write("  \r\n");
      out.write("    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n");
      out.write("      <ul class=\"navbar-nav mr-auto\">\r\n");
      out.write("        <li class=\"nav-item active\">\r\n");
      out.write("          <a class=\"nav-link\" href=\"/padelUnion\">Inicio</a>\r\n");
      out.write("        </li>\r\n");
      out.write("    \r\n");
      out.write("        <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            CalculaTest\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/padelUnion/calculanivel/edit.jsp\">Listado</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("\r\n");
      out.write("        <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            Partidos\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/padelUnion/partido/list.jsp\">Listado</a>\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/padelUnion/partido/create.jsp\">Nuevo Partido</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("\r\n");
      out.write("      \r\n");
      out.write("      </ul>\r\n");
      out.write("   \r\n");
      out.write("    </div>\r\n");
      out.write("  </nav>\r\n");
      out.write("  ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"container\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"row\">\r\n");
      out.write("<div class=\"col\">\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<script src=\"../js/Chart.js\"></script>\r\n");
      out.write("\r\n");
      out.write("<div class=\"item html\">\r\n");
      out.write("    <h2>HTML</h2>\r\n");
      out.write("    <svg width=\"160\" height=\"160\" xmlns=\"http://www.w3.org/2000/svg\">\r\n");
      out.write("     <g>\r\n");
      out.write("      <title>Layer 1</title>\r\n");
      out.write("      <circle id=\"circle\" class=\"circle_animation\" r=\"69.85699\" cy=\"81\" cx=\"81\" stroke-width=\"8\" stroke=\"#6fdb6f\" fill=\"none\"/>\r\n");
      out.write("     </g>\r\n");
      out.write("    </svg>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("<div class=\"item css\">\r\n");
      out.write("    <h2>CSS</h2>\r\n");
      out.write("    <svg width=\"160\" height=\"160\" xmlns=\"http://www.w3.org/2000/svg\">\r\n");
      out.write("     <g>\r\n");
      out.write("      <title>Layer 1</title>\r\n");
      out.write("      <circle id=\"circle\" class=\"circle_animation\" r=\"69.85699\" cy=\"81\" cx=\"81\" stroke-width=\"8\" stroke=\"#69aff4\" fill=\"none\"/>\r\n");
      out.write("     </g>\r\n");
      out.write("    </svg>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("<canvas id=\"oilChart\" width=\"600\" height=\"400\"></canvas>\r\n");
      out.write("<script src=\"../js/donut.js\"></script>\r\n");
      out.write("<table class=\"table\" id=\"tabla_listado\">\r\n");
      out.write("  <thead>\r\n");
      out.write("    <tr>\r\n");
      out.write("\r\n");
      out.write("      \r\n");
      out.write("    </tr>\r\n");
      out.write("  </thead>\r\n");
      out.write("  <tbody>\r\n");
      out.write("\r\n");
      out.write("  </tbody>\r\n");
      out.write("</table>\r\n");
      out.write("<a href=\"/padelUnion/calculanivel/funcionamiento.jsp\" class=\"btn btn-sm btn-primary\">Ver funcionamiento</a>\r\n");
      out.write("<p></p>\r\n");
      out.write("<a href=\"/padelUnion/calculanivel/edit.jsp\" class=\"btn btn-sm btn-primary\">Calcula mi nivel</a>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("    <!-- jQuery first, then Popper.js, then Bootstrap JS -->\r\n");
      out.write("    <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js\" integrity=\"sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js\" integrity=\"sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write(" \r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
