package org.apache.jsp.calculanivel;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import com.sjava.web.*;

public final class funcionamiento_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList(1);
    _jspx_dependants.add("/parts/menu.html");
  }

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.AnnotationProcessor _jsp_annotationprocessor;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_annotationprocessor = (org.apache.AnnotationProcessor) getServletConfig().getServletContext().getAttribute(org.apache.AnnotationProcessor.class.getName());
  }

  public void _jspDestroy() {
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<!DOCTYPE html>\r\n");
      out.write("<html lang=\"es-ES\">\r\n");
      out.write("<head>\r\n");
      out.write("    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\r\n");
      out.write("    <title>PadelUnion App</title>\r\n");
      out.write("    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css\" integrity=\"sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4\" crossorigin=\"anonymous\">\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"/padelprueba/css/estilos.css\">\r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" href=\"/padelprueba/css/estilosdavid.css\">\r\n");
      out.write("</head>\r\n");
      out.write("<body>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<nav class=\"navbar navbar-expand-lg  navbar-dark bg-primary\">\r\n");
      out.write("    <a class=\"navbar-brand\" href=\"#\">PadelUnionApp</a>\r\n");
      out.write("    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n");
      out.write("      <span class=\"navbar-toggler-icon\"></span>\r\n");
      out.write("    </button>\r\n");
      out.write("  \r\n");
      out.write("    <div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">\r\n");
      out.write("      <ul class=\"navbar-nav mr-auto\">\r\n");
      out.write("        <li class=\"nav-item active\">\r\n");
      out.write("          <a class=\"nav-link\" href=\"/padelUnion\">Inicio</a>\r\n");
      out.write("        </li>\r\n");
      out.write("    \r\n");
      out.write("        <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            CalculaTest\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/padelUnion/calculanivel/edit.jsp\">Listado</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("\r\n");
      out.write("        <li class=\"nav-item dropdown\">\r\n");
      out.write("          <a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\r\n");
      out.write("            Partidos\r\n");
      out.write("          </a>\r\n");
      out.write("          <div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/padelUnion/partido/list.jsp\">Listado</a>\r\n");
      out.write("            <a class=\"dropdown-item\" href=\"/padelUnion/partido/create.jsp\">Nuevo Partido</a>\r\n");
      out.write("          </div>\r\n");
      out.write("        </li>\r\n");
      out.write("\r\n");
      out.write("      \r\n");
      out.write("      </ul>\r\n");
      out.write("   \r\n");
      out.write("    </div>\r\n");
      out.write("  </nav>\r\n");
      out.write("  ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div>\r\n");
      out.write("<h2>Valoración de tu Nivel de Padel</h2>\r\n");
      out.write("<p>Esta herramienta es para conocer<strong>tu nivel de padel.</strong> </p>\r\n");
      out.write(" <p>El sistema consiste en valorar el diferente grado de progresión en el control de diferentes tipos de golpe\r\n");
      out.write(" para ponderar de una forma lo más objetiva posible cual es tu nivel de juego.</p>\r\n");
      out.write("<p>Una de las grandes dudas que tenemos cuando empezamos a jugar a pádel y vamos mejorando nuestra técnica es conocer nuestro nivel real de juego,\r\n");
      out.write(" ya que sobre todo para jugar torneos y partidos con jugadores desconocidos, en general, garantiza el equilibro de los partidos y competiciones.\r\n");
      out.write(" Llegando a ser más divertidos y efectivos.</p>\r\n");
      out.write("<p>Las diferencias de criterio entre clubs así como también la percepción subjetiva que tenemos de nuestro nivel de juego de padel,\r\n");
      out.write(" genera situaciones en que acabamos jugando con jugadores con niveles superiores al nuestro o al revés, \r\n");
      out.write(" jugando con personas que tiene un nivel muy inferior al que dicen tener.</p>\r\n");
      out.write("<p></p>\r\n");
      out.write("<p></p>\r\n");
      out.write("<h3>Sistemas de valoración</h3>\r\n");
      out.write("<p>Básicamente se utilizan tres sistemas de valoración dependiendo de los clubs o zonas en la que practiques Padel:</p>\r\n");
      out.write("<ul>\r\n");
      out.write("  <li><strong>El sistema de nivel de padel por letras:</strong> D,  C, C+, B, B+, A, (A) . Donde <strong>D</strong> corresponde a iniciación y<strong> A</strong> a profesional (máximo)</li>\r\n");
      out.write("  <li><strong>Sistema numérico</strong>: de nivel <strong>1</strong> hasta nivel <strong>7</strong>. 1 corresponde a iniciación y 7 a profesional</li>\r\n");
      out.write("  <li><strong>Sistema de Categorias:</strong>\r\n");
      out.write("  <ul>\r\n");
      out.write("    <li>Iniciación</li>\r\n");
      out.write("    <li>Intermedio</li>\r\n");
      out.write("    <li>Intermedio alto</li>\r\n");
      out.write("    <li>Avanzado</li>\r\n");
      out.write("    <li>Competición</li>\r\n");
      out.write("    <li>Profesional</li>\r\n");
      out.write("    </ul>\r\n");
      out.write("  </li>\r\n");
      out.write("</ul>\r\n");
      out.write("<p>Normalmente cuando empezamos a jugar a padel nuestro nivel es el de iniciación, \r\n");
      out.write("aunque existen factores previos que pueden modificar nuestro nivel de inicio, \r\n");
      out.write("como por ejemplo una experiencia previa en tenis, nuestra facilidad de aprencizaje, etc.</p>\r\n");
      out.write("\r\n");
      out.write("<p><strong>Funcionamiento:</strong></p>\r\n");
      out.write("<p>En función de la experiencia, entrenamiento, partidos jugados, forma física, reflejos, \r\n");
      out.write("experiencia en torneos y competiciones, nuestra progresión puede ser más rápida o \r\n");
      out.write("más lenta y por lo tanto nuestro nivel de juego puede evolucionar de una forma u\r\n");
      out.write(" otra con respecto a otros jugadores</p>\r\n");
      out.write("<p>Utilizamos un cuestionario para que sea el propio jugador \r\n");
      out.write("el que se autocalifique en diferentes facetas de la técnica del pádel y sus estilo de juego.</p>\r\n");
      out.write("<p>Cada pregunta dispone de varias opciones que equivalen a un grado de progreso del jugador.\r\n");
      out.write(" Una vez rellenado y enviado el formulario, la herramienta calcula mediante un alogoritmo,  \r\n");
      out.write(" unos valores númericos que finalemente tienen una traducción en un nivel de padel equivalente.\r\n");
      out.write(" Este algoritmo puede ser modificado, siempre que mejore la calidad del mismo con cualquiera de futuras propuestas</p>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<div class=\"alert alert-info\" role=\"alert\">\r\n");
      out.write("<p>Lógicamente para que el resultado sea fiable y preciso es necesario que el jugador / juagadora \r\n");
      out.write("sea objetivo en sus respuestas y seleecione realmente aquel escenario que mejor define su situación \r\n");
      out.write("real de juego en la pista.</p>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("<a href=\"/padelUnion/calculanivel/edit.jsp\" class=\"btn btn-sm btn-primary\">Calcula mi nivel</a>\r\n");
      out.write("\r\n");
      out.write("  <!-- jQuery first, then Popper.js, then Bootstrap JS -->\r\n");
      out.write("    <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js\" integrity=\"sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("    <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js\" integrity=\"sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm\" crossorigin=\"anonymous\"></script>\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("</body>\r\n");
      out.write("</html>\r\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
