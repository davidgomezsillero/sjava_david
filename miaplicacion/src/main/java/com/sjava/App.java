package com.sjava;

public class App 
{
    public static void main( String[] args )
    {
        new Persona("ana", 1995, "ana@gmail.com");
        new Persona("claudia", 1999, "claudia@gmail.com");
        new Persona("juan", 1980, "juan@gmail.com");
        new Persona("pepe", 1999, "pepe@gmail.com");
        new Persona("manolo", 2000, "manolo@gmail.com");

       PersonaController.muestraContactos();
       System.out.println("Muestra 2...");
       PersonaController.muestraContactosId(2);

       System.out.println("-------------");
       System.out.println("Eliminando 4...");
       System.out.println("-------------");

       PersonaController.borraContactoId(4);
       PersonaController.muestraContactos();
    }
}
