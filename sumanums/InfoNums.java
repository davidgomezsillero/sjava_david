class InfoNums {
    public static void main(String[] args) {
        int total=0;
        int mayor=Integer.parseInt(args[0]);
        int menor=Integer.parseInt(args[0]);
        int i = 0;
        for (String s : args){
            int num = Integer.parseInt(s);
            
            if(num>mayor){
                mayor=num;
            }
            if(num<menor){
                menor=num;
            }
            total += num;
            i++;
        }
        double media=0;
        media=(double)total/i;
        System.out.println(i + " numeros introducidos\n");
        System.out.println("Numero mayor "+mayor);
        System.out.println("Numero menor "+menor);
        System.out.println("Media aritmetica "+ media );
    }
}