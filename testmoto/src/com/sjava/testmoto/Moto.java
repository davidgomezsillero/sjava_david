package com.sjava.testmoto;

class Moto {
    String marca;
    String modelo;
    int cc;
    int cv;
    int pvp;

    public Moto(String marca, String modelo, int cc, int cv, int pvp) {
        this.marca = marca;
        this.modelo = modelo;
        this.cc = cc;
        this.cv = cv;
        this.pvp = pvp;
    }

    public void comparaCv(Moto otra){
        if (this.cv > otra.cv) {
            System.out.printf(
                "La %s %s es más potente que la %s %s\n",
                this.marca, this.modelo, otra.marca, otra.modelo);
        } else {
            System.out.printf(
                "La %s %s es más potente que la %s %s\n",
                otra.marca, otra.modelo, this.marca, this.modelo);   
        }
    }

    public void comparaPrecio(Moto otra){
        if (this.pvp > otra.pvp) {
            System.out.printf(
                "La %s %s es más barata que la %s %s\n",
                this.marca, this.modelo, otra.marca, otra.modelo);
        } else {
            System.out.printf(
                "La %s %s es más cara que la %s %s\n",
                otra.marca, otra.modelo, this.marca, this.modelo);   
        }
        
    }
}

