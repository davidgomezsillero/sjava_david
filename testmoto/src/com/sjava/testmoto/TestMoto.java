package com.sjava.testmoto;

class TestMoto {
    public static void main(String[] args) {
        Moto moto1 = new Moto("Yamaha", "MT07", 689, 75, 6799);
        Moto moto2 = new Moto("Vespa", "Ad 10", 200, 20, 1250);
        Moto moto3 = new Moto("Kawasaki", "Ninja 650", 649, 68, 7250);

        moto1.comparaCv(moto2);
        moto1.comparaPrecio(moto2);
        moto2.comparaPrecio(moto3);
        moto3.comparaPrecio(moto1);
    }
}
