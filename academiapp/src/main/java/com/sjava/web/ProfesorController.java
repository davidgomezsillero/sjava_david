package com.sjava.web;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

/* datos en base de datos */
public class ProfesorController {

    //private static List<Profesor> listaProfesores = new ArrayList<Profesor>();
      // constantes utilizadas en las ordenes sql
	private static final String TABLE = "profesores";
	private static final String KEY = "idprofesores";
    private static int contador = 0;

    static {

        // Profesor a = new Profesor(99,"ricard hernández", "algo@algo.com", "999333");
        // listaProfesores.add(a);
    }

    
    // getAll devuelve todos los registros de la tabla
    public static List<Profesor> getAll(){
        
        List<Profesor> listaProfesores = new ArrayList<Profesor>();
		String sql = String.format("select %s,nombre,email,telefono,especialidad from %s", KEY, TABLE);
		//String sql = "select id,nombre,password from "+TABLE;
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {

			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				Profesor u = new Profesor(
                    (Integer) rs.getObject(1),
                    (String) rs.getObject(2),
                    (String) rs.getObject(3),
                    (String) rs.getObject(4),
                    (String) rs.getObject(5));
				listaProfesores.add(u);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return listaProfesores;

    }

    //getId devuelve un registro
    public static Profesor getId(int id){
        Profesor u = null;
        String sql = String.format("select %s,nombre,email,telefono,especialidad from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Profesor(
                (Integer) rs.getObject(1),
                (String) rs.getObject(2),
                (String) rs.getObject(3),
                (String) rs.getObject(4),
                (String) rs.getObject(5)
                );
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }
   
    //save guarda un profesor
    // si es nuevo (id==0) lo añade a la BDD
    // si ya existe, actualiza los cambios
    public static void save(Profesor p) {
        String sql;
        if (p.getId()>0) {
            sql = String.format("UPDATE %s set nombre=?, email=?, telefono=?, especialidad=? where %s=%d", TABLE, KEY, p.getId());
        }else {
            sql = String.format("INSERT INTO %s (nombre, email, telefono, especialidad) VALUES (?,?,?,?)", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, p.getNombre());
            pstmt.setString(2, p.getEmail());
            pstmt.setString(3, p.getTelefono());
            pstmt.setString(4, p.getEspecialidad());
            pstmt.executeUpdate();
        if (p.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    p.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
 

    // removeId elimina profesor por id
    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}