import java.util.Scanner;

class PideNums2 {

    public static void main(String[] args) {
    
        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        int num=0;
        boolean comprobar = false;
        do {
            comprobar=false;
            System.out.printf("Entra un num: ");
            try {  
                    num = keyboard.nextInt(); 
                    total += num; 
                } catch (Exception e) {  
                    System.out.println("***Dato incorrecto - 0 para salir***");
                    keyboard.next();
                    comprobar=true;
                } 
        } while (num>0 || comprobar==true);
        System.out.println("La suma es "+ total);
        keyboard.close();
    }
    
}
