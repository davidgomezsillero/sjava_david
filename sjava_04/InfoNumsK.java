import java.util.Scanner;

class InfoNumsK {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);
        int total = 0;
        boolean comprobar = true;
        int mayor = 0;
        int menor = 0;
        int num = 1;
        int i = 0;

        do {

            System.out.printf("Entra un num: ");
            try {
                num = keyboard.nextInt();
                if (num == 0) {
                } else 
                    if (comprobar) {
                        mayor = num;
                        menor = num;
                        comprobar = false;
                        i++;
                    } else {

                        if (num > mayor) {
                            mayor = num;
                        }
                        if (num < menor) {
                            menor = num;
                        }
                        
                        i++;
                    }
                    total += num;
                    
                   
                
            } catch (Exception e) {
                System.out.println("***Dato incorrecto - 0 para salir***");
                keyboard.next();
                // comprobar = true;
            }
        } while (num != 0);
        System.out.println("La suma es " + total);
        double media = 0;
        media = (double) total / i;
        System.out.println(i + " numeros introducidos\n");
        System.out.println("Numero mayor " + mayor);
        System.out.println("Numero menor " + menor);
        System.out.println("Media aritmetica " + media);
        keyboard.close();
    }

}