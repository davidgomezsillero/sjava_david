class Person {
    String nombre;
    int edad;

    // void presentacion(){

    //     System.out.println("Hola me llamo " + nombre);
    // }

    public Person(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
     
    }

    public void comparaEdad(Person persona){
        if (this.edad > persona.edad) {
            System.out.printf(
                " %s es más grande que %s\n",
                this.nombre, persona.nombre);
        } else {
            System.out.printf(
                " %s es más pequeña que %s\n",
                this.nombre, persona.nombre); 
        }
    }
}
