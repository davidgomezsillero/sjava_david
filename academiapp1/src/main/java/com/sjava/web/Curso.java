package com.sjava.web;

public class Curso {

    private int id;
    private String nombre;
    private int duracion;
    private int idProfesor;

    public Curso(String nombre, int duracion, int idProfesor) {
        this.nombre = nombre;
        this.duracion = duracion;
        this.idProfesor = idProfesor;
        
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de AlumnoController
        // AlumnoController.nuevoContacto(this);
    }

    public Curso(int id, String nombre,int duracion, int idProfesor) {
        this.id = id;
        this.nombre = nombre;
        this.duracion = duracion;
        this.idProfesor = idProfesor;
        
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre){
        this.nombre = nombre;
    }

    public int getDuracion(){
        return this.duracion;
    }

    protected void setDuracion(int duracion){
        this.duracion = duracion;
    }


    public int getId(){
        return this.id;
    }

    protected void setId(int id){
        this.id=id;
    }

    public int getIdProfesor(){
        return this.idProfesor;
    }

    protected void setIdProfesor(int idProfesor){
        this.idProfesor=idProfesor;
    }

   
    @Override
    public String toString() {
        return String.format("%s (%s)", this.nombre, this.duracion);
    }
  
}