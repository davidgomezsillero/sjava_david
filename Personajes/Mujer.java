
class Mujer extends Persona{


    //Puede recibir o no, el obligatorio es que le mande 
        //al ancestro que si los pide. con args en super()

    public Mujer(String nombre, int edad){

            super(nombre,edad);

    }

    @Override
    public String getNombre(){
    
        String nuevonombre = "Sra " + this.nombre;
        return nuevonombre;
    }
    @Override
    public int getEdad(){
        if(this.edad>45){
                return this.edad-5;

        }else{
            return this.edad;
        }
       // return (this.edad>45) ? this.edad-5 : this.edad;
    }

}