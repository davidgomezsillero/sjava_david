
class Hombre extends Persona{

        //Puede recibir o no, el obligatorio es que le mande 
        //al ancestro que si los pide. con args en super()
    public Hombre(String nombre, int edad){

        super(nombre, edad);
    }

    @Override
    public String getNombre(){
        
		String nuevonombre = "Sr " + this.nombre;
        return nuevonombre;
    }
}