

class Persona{

    protected String nombre;
    protected int edad;

    public  Persona(String nombre, int edad){
        
        this.nombre=nombre;
        this.edad = edad;

    }
     public String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre){

        this.nombre=nombre;
    }
    public int getEdad(){
        return edad;
    }

    public void setEdad(Integer edad){

        this.edad=edad;
    }

    public String presentacion(){
        String presenta;
                    //getNombre() y getEdad() de la clase
		presenta = "Hola, soy "+ this.getNombre() + " y tengo "+ this.getEdad() +" años";
        return presenta;
    }
}