import java.util.Date;
import java.util.Calendar;
import java.text.*;

class DateUtil {

    // constructor privado para no dar opcion a crear new DataUtil() en el main
    private DateUtil() {
    }

    // Metodo para crear con calendar
    public static Date createDate(int year, int month, int day) {

        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, day);

        Date fecha = cal.getTime();

        return fecha;
    }

    public static Date createDateTime(int year, int month, int day, int hour, int minutes, int seconds) {

        Calendar cal = Calendar.getInstance();
        cal.set(year, month - 1, day, hour, minutes, seconds);

        Date fecha = cal.getTime();

        return fecha;
    }

    public static String formatDate(Date date, String pattern) {

        // long ts = cal.getTimeInMillis();

        // Formato de fechas
        // Con getDisplay() tb podremos hacerlo

        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String str = sdf.format(date);
        return str;
    }

    public static int sundayCount(int month, int year) {

        int num = 0;

        Calendar c = Calendar.getInstance();
        c.set(year,month-1,1);
        // c.set(Calendar.MONTH,month);
        // c.set(Calendar.YEAR,year);
        // c.set(Calendar.day);
        while(c.get(Calendar.MONTH)==month-1){

            if(c.get(Calendar.DAY_OF_WEEK)==1){
                System.out.println(c.getTime());
                num++;
            }
            c.add(Calendar.DATE,1);
        }
       // Date fecha = c.getTime();
       
        return num;
    }

}
// Ejemplos de datos a tratar

// String strDate = "03/05/2013 18:34";
// SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
// Date date2 = sdf.parse(strDate);

// Date date3 = new Date();
// SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM-yy HH:mm");
// String str = sdf2.format(date3);