import java.util.Date;
import java.text.*;

class DateUtilTest {
    public static void main(String[] args) {

        // DateUtil fecha;
        // Fecha para pasarle a DateUtil (personalizada sin pedir datos)
        // Ejemplos de meter datos para fecha

        // Llamar al metodo de DataUtil
        // para mostrar las pruebas de fecha
        Date fecha = DateUtil.createDate(2012, 6, 9);
        Date d = DateUtil.createDateTime(2012, 8, 10, 39, 44, 12);
        

        System.out.println(fecha);
        System.out.println(d);


        // Llamar al metodo de DataUtil
        // para mostrar formato
        System.out.println("Esta es: " + DateUtil.formatDate(fecha, "dd/MM/yyyy"));

          // Llamar al metodo de DataUtil
        // para mostrar cantidad de domingo por mes
        System.out.println("Estos son los domingos: ");
        int contador = DateUtil.sundayCount(3,2012);
        System.out.println(contador);


        // PRUEBAS
        System.out.println("");
        System.out.println("PRUEBAS");
        System.out.println("=======");
        Date fecha2 = new Date();
        System.out.println(fecha2);

        // Formato de fechas
        // Con getDisplay() tb podremos hacerlo

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String str = sdf.format(fecha2);
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd:MMM");
        String str2 = sdf2.format(fecha);
        // fecha2 = sdf.parse(str);
        // Mostrar prueba
        System.out.println(str);
        System.out.println(str2);

    }
}